SET(BUILD_PREFIX ${CMAKE_BINARY_DIR}/external_build)
SET(EXT_INSTALL ${CMAKE_BINARY_DIR}/ext_install)

##### SETUP INTERNAL LIBRARY ACTIVATION #####
SET(INTERNAL_BOOST OFF)
SET(INTERNAL_GENGETOPT OFF)
SET(INTERNAL_EIGEN OFF)

SET(BOOST_URL "http://sourceforge.net/projects/boost/files/boost/1.65.1/boost_1_65_1.tar.gz/download" CACHE STRING "URL to download Boost from")
mark_as_advanced(BOOST_URL)

SET(CONFIGURE_CPP_FLAGS "${EXTRA_CPP_FLAGS}")
SET(CONFIGURE_LD_FLAGS "${EXTRA_LD_FLAGS}")



##################
# Build BOOST
##################

if (INTERNAL_BOOST)
  SET(BOOST_SOURCE_DIR ${BUILD_PREFIX}/boost-prefix/src/boost)
  ExternalProject_Add(boost
    URL ${BOOST_URL}
    PREFIX ${BUILD_PREFIX}/boost-prefix
    CONFIGURE_COMMAND
           ${BOOST_SOURCE_DIR}/bootstrap.sh --prefix=${CMAKE_BINARY_DIR}/ext_build/boost
    BUILD_IN_SOURCE 1
    BUILD_COMMAND ${BOOST_SOURCE_DIR}/b2 --with-exception
    INSTALL_COMMAND echo "No install"
  )
  set(Boost_INCLUDE_DIRS ${BOOST_SOURCE_DIR} CACHE STRING "Boost path" FORCE)
  set(Boost_LIBRARIES ${BOOST_SOURCE_DIR}/stage/lib/libboost_python.a CACHE STRING "Boost libraries" FORCE)
ELSE()
  find_package(Boost 1.63.0 REQUIRED)
endif()
mark_as_advanced(Boost_INCLUDE_DIRS Boost_LIBRARIES)


#SET(OMPTL_BUILD_DIR ${CMAKE_BINARY_DIR}/omptl-prefix/src/omptl)
#ExternalProject_Add(omptl
#  URL ${CMAKE_SOURCE_DIR}/external/omptl-20120422.tar.bz2
#  CONFIGURE_COMMAND echo "No configure"
#  BUILD_COMMAND echo "No build"
#  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory ${OMPTL_BUILD_DIR} ${CMAKE_BINARY_DIR}/external/stage/include/omptl
#)

find_program(PATCH patch)

include_directories(${OMPTL_BUILD_DIR}/.. ${CMAKE_SOURCE_DIR}/splotch)


#include_directories(${EXT_INSTALL}/include ${EIGEN_INCLUDE_PATH}
#                    ${Boost_INCLUDE_DIRS})
