# README #

This README would normally document whatever steps are necessary to get your application up and running.

Author: Guilhem Lavaux (guilhem.lavaux@iap.fr)

Splotch is bundled in the repository, please check the license/author for more details.

### License

This software is license under CECILL 2.1 (https://spdx.org/licenses/CECILL-2.1.html)

See LICENSE.txt for details.

### What is this repository for? ###

This is PySplotch a python wrapper for Splotch a rendering tool for particle simulation by Klaus Dolag. It requires version 4.4 of splotch which is already bundled.

### How do I get set up? ###

PySplotch is based on cmake for building and installing. But for convenience, I have added a shell script that automate and
make explicit the build options. This script is called 'build.sh' and must be invoked from the top source directory. To get
the options you can invoke with `./build.sh --help`. The lists are the following:

Option                    | Description
------------------------- | -----------
`--cmake CMAKE_BINARY`    | instead of searching for cmake in the path, use the indicated binary
`--python PYTHON_BIN`     | force the python interpreter to use
`--without-openmp`        | build without openmp support (default with)
`--with-mpi`              | build with MPI support (default without)
`--c_compiler COMPILER`   | specify the C compiler to use (default to cc)
`--cxx_compiler COMPILER` | specify the CXX compiler to use (default to c++)
`--build-dir DIRECTORY`   | specify the build directory (default to "build/" )
`--debug`                 | build for full debugging
`--user`                  | install in the user site-package
`--extra_flags FLAGS`     | extra flags to pass to cmake
`--download-deps`         | Predownload dependencies
`--use-predownload`       | Use the predownloaded dependencies. They must be in downloads/


### How to run it ###

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

For any questions, suggestions or patches please contact Guilhem Lavaux <lavaux@iap.fr>
