from cython.operator cimport dereference as deref, preincrement as inc
from cython.parallel cimport prange
from cpython cimport PyObject, Py_INCREF
cimport numpy as npx
import numpy as np
from libcpp.vector cimport vector
from libcpp.map cimport map as cppmap
from libcpp.string cimport string as cppstring
from libcpp cimport bool as cppbool
cimport cython

ctypedef cppmap[cppstring,cppstring] keypair

npx.import_array()

cdef class ArrayWrapper:
    cdef void* data_ptr
    cdef npx.uint64_t size
    cdef int type_array

    cdef set_data(self, npx.uint64_t size, int type_array, void* data_ptr):
        """ Set the data of the array

This cannot be done in the constructor as it must recieve C-level
arguments.

Parameters:
-----------
size: int
Length of the array.
data_ptr: void*
Pointer to the data

    """
        self.data_ptr = data_ptr
        self.size = size
        self.type_array = type_array

    def __array__(self):
        """ Here we use the __array__ method, that is called when numpy tries to get an array from the object."""
        cdef npx.npy_intp shape[1]

        shape[0] = <npx.npy_intp> self.size
        # Create a 1D array, of length 'size'
        ndarray = npx.PyArray_SimpleNewFromData(1, shape, self.type_array, self.data_ptr)
        Py_INCREF(self)
        npx.PyArray_SetBaseObject(ndarray, self)
        return ndarray

    def __dealloc__(self):
        """ Frees the array. This is called by Python when all the references to the object are gone. """
        pass

cdef object wrap_array(void *p, npx.uint64_t s, int typ):
    cdef npx.ndarray ndarray
    cdef ArrayWrapper wrapper

    wrapper = ArrayWrapper()
    wrapper.set_data(s, typ, p)
    ndarray = npx.ndarray(wrapper, copy=False)

    return ndarray


cdef object wrap_float_array(float *p, npx.uint64_t s):
    return wrap_array(<void *>p, s, npx.NPY_FLOAT32)

cdef object wrap_int64_array(npx.int64_t* p, npx.uint64_t s):
    return wrap_array(<void *>p, s, npx.NPY_INT64)

cdef object wrap_int_array(int* p, npx.uint64_t s):
    return wrap_array(<void *>p, s, npx.NPY_INT)

cdef extern from "cxxsupport/vec3.h":

    cppclass vec3:
        vec3()
        vec3(float,float,float)

        void Set(float,float,float)


cdef extern from "cxxsupport/arr.h":

    cppclass arr2[T]:
        arr2()
        arr2(int,int)

        T& operator[](int,int)
        T* operator[](int)
        int size()
        int size1()
        int size2()

cdef extern from "cxxsupport/paramfile.h":

    cppclass paramfile:
        paramfile()
        paramfile(keypair&)
        void setVerbosity(cppbool)


cdef extern from "kernel/colour.h":

    cppclass RGB_tuple[T]:
        T r, g, b

        RGB_tuple()
        RGB_tuple(T,T,T)

        RGB_tuple& operator[](T)
        RGB_tuple operator+(RGB_tuple&)

    ctypedef RGB_tuple[float] COLOUR

cdef extern from "kernel/colourmap.h":
    cppclass anythingMap[T]:

        anythingMap()
        void addVal(float,T&)
        void sortMap()

    ctypedef anythingMap[COLOUR] COLOURMAP

cdef extern from "splotch/splotchutils.h":

    cppclass particle_sim:
        float x, y, z, r, I
        int type
        int active
        COLOUR e

        particle_sim()
        particle_sim(COLOUR&,float x,float y,float z,float r,float I,int ptype, int active)

    cppclass exptable[T]:
        exptable (T maxexp)
        T operator() (T arg)
        T expm1(T arg)


cdef extern from "splotch/splotch_host.h":
    ctypedef void (*print_func)(const char *msg)
    void host_rendering (paramfile&, vector[particle_sim] &particle_data,
                  arr2[COLOUR] &pic, vec3 &campos, vec3 &lookat, vec3 &sky,
                    vector[COLOURMAP] &amap, print_func ufunc) nogil

cdef class SplotchColormap:

    cdef vector[COLOURMAP] cmap
    cdef int num_types

    def __init__(self,*args,**kwargs):
        if len(args)>0:
          raise RuntimeError("Constructor does not take arguments")

        self.num_types = kwargs.get('num_types', 1)
        if (self.num_types <= 0):
            raise RuntimeError("Number of types must be positive")

        self.cmap.resize(self.num_types)

    def addValue(self, int ptype, float x, tuple rgb):
        """addValue(particle_type, x, (r, g, b))

        0 < x < 1
        0 < r,g,b < 1

        """
        cdef COLOUR col
        cdef float r, g, b

        r,g,b = rgb

        col = COLOUR(r,g,b)

        assert ptype >= 0
        assert ptype < self.num_types
        assert x >= 0 and x <= 1
        assert r >= 0 and r <= 1
        assert g >= 0 and g <= 1
        assert b >= 0 and b <= 1
        self.cmap[ptype].addVal(x, col)

cdef class SplotchCamera:

    cdef vec3 campos
    cdef vec3 lookat
    cdef vec3 sky
    cdef float fov

    def __init__(self,pos,lookat,sky, fov=45):
        """ __init__(campos, lookat, sky, fov=45)
        """
        self.campos = vec3(pos[0],pos[1],pos[2])
        self.lookat = vec3(lookat[0],lookat[1],lookat[2])
        self.sky = vec3(sky[0],sky[1],sky[2])
        self.fov = fov

cdef class ArrColour_ArrayWrapper(ArrayWrapper):

    cdef arr2[COLOUR] *arr

    def __init__(self):
        self.arr = <arr2[COLOUR]*>0

    def __dealloc__(self):
        if self.arr != <arr2[COLOUR]*>0:
            del self.arr


cdef object _wrap_arr2_COLOUR(arr2[COLOUR] *a):
    cdef npx.ndarray ndarray
    cdef ArrColour_ArrayWrapper wrapper

    wrapper = ArrColour_ArrayWrapper()
    wrapper.set_data(a.size()*3, npx.NPY_FLOAT32, <void *>(deref(a)[0]))
    wrapper.arr = a
    ndarray = np.array(wrapper, order='C', copy=False)

    return ndarray.reshape(a.size2(), a.size1(), 3)

cdef class particleArray:

    cdef vector[particle_sim] *particle_data
    cdef int maxTypes

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def __init__(self,numParticles):
        """__init__(numParticles)
        """
        cdef ssize_t i

        self.particle_data = new vector[particle_sim]()
        self.particle_data.resize(numParticles)
        self.maxTypes = 1

        with nogil:
          for i in prange(self.particle_data.size()):
              deref(self.particle_data)[i].type = 0
              deref(self.particle_data)[i].active = 1

    def __dealloc__(self):
        del self.particle_data

    property numParticles:
      def __get__(self):
        return self.particle_data.size()

    def resize(self,numParticles):
        """resize(int numParticles)
        """
        cdef ssize_t oldsize = self.particle_data.size()
        cdef ssize_t i_numParts = numParticles
        cdef ssize_t i

        self.particle_data.resize(i_numParts)
        if oldsize < i_numParts:
          with nogil:
            for i in prange(oldsize, i_numParts):
              deref(self.particle_data)[i].type = 0
              deref(self.particle_data)[i].active = 1

    @cython.wraparound(False)
    def setIntensity(self, object I, ssize_t  start=0, ssize_t end=-1, ssize_t stride=1):
        """setValues(r float value/array)
        """
        cdef ssize_t i, j, ns
        cdef float Ival
        cdef float[:] I_array

        if isinstance(I, float) or isinstance(I, int):
          Ival = I
          with nogil:
            for i in prange(self.particle_data.size()):
                deref(self.particle_data)[i].I = Ival
        elif isinstance(I, np.ndarray):
          if end < 0:
            end = self.particle_data.size()
          end = min(end,self.particle_data.size())
          ns = <ssize_t>((end-start)/stride)
          I_array = I

          with nogil:
            for j in prange(ns):
                i = start + j*stride
                deref(self.particle_data)[i].I = I_array[j]
        else:
          raise RuntimeError("Invalid value given to setIntensity (must be a scalar or an ndarray)")

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def setOneRadius(self, float r):
        """setValues(r float value)
        """
        cdef ssize_t i

        with nogil:
          for i in prange(self.particle_data.size()):
              deref(self.particle_data)[i].r = r

    @cython.wraparound(False)
    @cython.cdivision(True)
    def setRadius(self, object r, ssize_t start=0, ssize_t end=-1, ssize_t stride=1):
        """setRadius(r array, start=0, end=-1, stride=1)
        """
        cdef ssize_t i, j, ns
        cdef float[:] r_array

        if not isinstance(r, np.ndarray):
            raise RuntimeError("r must be a numpy array")

        if end < 0:
          end = self.particle_data.size()

        end = min(end,self.particle_data.size())
        ns = (end-start)/stride
        r_array = r
        with nogil:
          for j in prange(ns):
              i = start + stride*j
              deref(self.particle_data)[i].r = r_array[j]

    def getRadius(self):
        cdef npx.float32_t[:] arrvals
        cdef object ret
        cdef ssize_t ns, i

        ns = self.particle_data.size()

        arrvals = ret = np.empty((ns,), dtype=np.float32)
        with nogil:
          for i in prange(self.particle_data.size()):
            arrvals[i] = deref(self.particle_data)[i].r

        return ret

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def setOneValue(self, float r):
        """setOneValue(r float value)
        """
        cdef ssize_t i

        with nogil:
          for i in prange(self.particle_data.size()):
              deref(self.particle_data)[i].e.r = r

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def setType(self, object t, ssize_t start=0, ssize_t end=-1, ssize_t stride=1):
        """setOneValue(t int value)
        """
        cdef ssize_t i, j, ns
        cdef npx.int32_t[:] t_array
        cdef int t0

        if end < 0:
          end = self.particle_data.size()
        end = min(end,self.particle_data.size())
        ns = <ssize_t>((end-start)/stride)

        if isinstance(t, int):
          t0 = t
          self.maxTypes = max(self.maxTypes, t0+1)
          with nogil:
            for j in prange(ns):
              i = start + j*stride
              deref(self.particle_data)[i].type = t0

        elif isinstance(t, np.ndarray) and t.dtype == np.int32:
          t_array = t
          with nogil:
            for j in prange(ns):
              i = start + j*stride
              deref(self.particle_data)[i].type = t_array[j]
              self.maxTypes = max(self.maxTypes, t_array[j]+1)

        else:
          raise RuntimeError("Invalid type array-or-scalar")

    @cython.wraparound(False)
    def setValues(self, object r, ssize_t start=0, ssize_t end=-1, ssize_t stride=1):
        """setValues(r array, start=0, end=-1, stride=1)
        """
        cdef ssize_t i, j, ns
        cdef float[:] v_array

        if not isinstance(r, np.ndarray):
            raise RuntimeError("r must be a numpy array")

        r = r.astype(dtype=np.float32,copy=False)

        if end < 0:
          end = self.particle_data.size()
        end = min(end,self.particle_data.size())
        ns = <ssize_t>((end-start)/stride)

        v_array = r
        for j in xrange(ns):
            i = start + j*stride
            deref(self.particle_data)[i].e.r = v_array[j]

    @cython.cdivision(True)
    @cython.wraparound(False)
    def __getitem__(self, index):
        cdef ssize_t start, stop, stride
        cdef ssize_t i, j, ns
        cdef npx.float32_t[:,:] arrvals
        cdef object ret

        if isinstance(index, slice):
            start,stop,stride = index.indices(self.particle_data.size())
            ns = (stop-start)/stride
            arrvals = ret = np.empty((ns,3), dtype=np.float32)
            for i in xrange(ns):
                j = start + i*stride
                arrvals[i,0] = deref(self.particle_data)[j].x
                arrvals[i,1] = deref(self.particle_data)[j].y
                arrvals[i,2] = deref(self.particle_data)[j].z
            return ret
        elif isinstance(index, tuple):
            raise RuntimeError("Multidimensional index is not supported")
        else:
            i = index
            return (deref(self.particle_data)[i].x,
                    deref(self.particle_data)[i].y,
                    deref(self.particle_data)[i].z)


    @cython.cdivision(True)
    @cython.wraparound(False)
    @cython.boundscheck(False)
    def __setitem__(self, index, vals):
        cdef ssize_t start, stop, stride
        cdef ssize_t i, j, ns
        cdef npx.float32_t[:,:] arrvals
        cdef npx.float32_t[:] arr_x, arr_y, arr_z

        if isinstance(index, slice):

            if isinstance(vals, list) or isinstance(vals,tuple):
              if len(vals) != 3:
                raise RuntimeError("If it is a list or tuple, it must have three elements")

              arr_x = np.array(vals[0], dtype=np.float32)
              arr_y = np.array(vals[1], dtype=np.float32)
              arr_z = np.array(vals[2], dtype=np.float32)

              start,stop,stride = index.indices(self.particle_data.size())
              ns = (stop-start)/stride
              with nogil:
                for i in prange(ns):
                    j = start + i*stride
                    deref(self.particle_data)[j].x = arr_x[i]
                    deref(self.particle_data)[j].y = arr_y[i]
                    deref(self.particle_data)[j].z = arr_z[i]
            else:
              arrvals = np.array(vals, dtype=np.float32)
              start,stop,stride = index.indices(self.particle_data.size())
              ns = (stop-start)/stride
              with nogil:
                for i in prange(ns):
                    j = start + i*stride
                    deref(self.particle_data)[j].x = arrvals[i,0]
                    deref(self.particle_data)[j].y = arrvals[i,1]
                    deref(self.particle_data)[j].z = arrvals[i,2]
        elif isinstance(index, tuple):
            raise RuntimeError("Multidimensional index is not supported")
        else:
            i = index
            deref(self.particle_data)[i].x = vals[0]
            deref(self.particle_data)[i].y = vals[1]
            deref(self.particle_data)[i].z = vals[2]

cdef extern from "cppstring_helper.h":

    cppstring convert_int_to_str(int)
    cppstring convert_float_to_str(float)

cdef class dataDescriptor:

  cdef float brightness
  cdef int color_log
  cdef int color_asinh
  cdef int intensity_log
  cdef float color_min, color_max
  cdef float intensity_min, intensity_max
  cdef int intensity_min_set, intensity_max_set

  def __init__(self):

    self.color_log = 0
    self.color_asinh = 0
    self.brightness = 1
    self.color_min = 0
    self.color_max = 1
    self.intensity_log = 0
    self.intensity_min_set = 0
    self.intensity_max_set = 0

  property intensity_min:
    def __set__(self, v):
      assert isinstance(v,float) or isinstance(v,int)
      self.intensity_min = v
      self.intensity_min_set = 1

    def __get__(self):
      if self.intensity_min_set:
        return self.intensity_min
      raise ValueError("No min intensity defined")

  property intensity_max:
    def __set__(self, v):
      assert isinstance(v,float) or isinstance(v,int)
      self.intensity_max = v
      self.intensity_max_set = 1

    def __get__(self):
      if self.intensity_max_set:
        return self.intensity_max
      raise ValueError("No max intensity defined")

  property brightness:
    def __set__(self, b):
      assert isinstance(b,float) or isinstance(b,int)
      self.brightness = b
    def __get__(self):
      return self.brightness

  property intensity_log:
    def __set__(self, b):
      assert isinstance(b,bool)
      self.intensity_log = 1 if b else 0

    def __get__(self):
      return True if self.intensity_log else False

  property color_log:
    def __set__(self, b):
      assert isinstance(b,bool)
      self.color_log = 1 if b else 0

    def __get__(self):
      return True if self.color_log else False

  property color_asinh:
    def __set__(self, b):
      assert isinstance(b,bool)
      self.color_asinh = 1 if b else 0

    def __get__(self):
      return True if self.color_asinh else False

  property color_min:
    def __set__(self, b):
      assert isinstance(b,float) or isinstance(b,int)
      self.color_min = b
    def __get__(self):
      return self.color_min

  property color_max:
    def __set__(self, b):
      assert isinstance(b,float) or isinstance(b,int)
      self.color_max = b
    def __get__(self):
      return self.color_max


def _intern_python_printer(const char *msg):
    cdef bytes bmsg = msg
    try:
      print(bmsg.decode('utf-8'))
    except:
      pass

cdef void python_printer(const char *msg) noexcept nogil:
  with gil:
    _intern_python_printer(msg)

def splotch(list particleSets,
            particleArray positions, SplotchCamera camera, SplotchColormap colormap,
            float gray_absorption=0.2,
            int xres=800, int yres=800, backup_positions=True,
            zmin=None, zmax=None):
    """
    Arguments:
      * particleSets  (list of dataDescriptor)
      * positions (particleArray)
      * camera (SplotchCamera)
      * colormap (SplotchColormap)

    Keyword Arguments:
      * gray_absorption (float, default 0.2)
      * xres (int, default 800)
      * yres (int, default 800)
      * backup_positions (bool, default true)
      * zmin (float, default None)
      * zmax (float, default None)
"""
    cdef keypair params
    cdef paramfile *pfile
    cdef arr2[COLOUR] *pic
    cdef int i, ix, iy
    cdef exptable[npx.float32_t] *xexp
    cdef vector[particle_sim] *use_positions
    cdef object a
    cdef npx.float32_t[:,:,::1] a_array
    cdef dataDescriptor infod

    params[b"a_eq_e"] = b"T"
    params[b"gray_absorption"] = convert_float_to_str(gray_absorption)
    params[b"ptypes"] = convert_int_to_str(positions.maxTypes)

    for i,info in enumerate(particleSets):
      if not isinstance(info, dataDescriptor):
        raise RuntimeError("Invalid data set descriptor")

      infod = info

      params[b"intensity_log%d" % i] = b"T" if infod.intensity_log else b"F"
      if infod.intensity_min_set:
        params[b"intensity_min%d" % i] = convert_float_to_str(infod.intensity_min)
      if infod.intensity_max_set:
        params[b"intensity_max%d" % i] = convert_float_to_str(infod.intensity_max)

      params[b"color_log%d" % i] = b"T" if infod.color_log else b"F"
      params[b"color_asinh%d" % i] = b"T" if infod.color_asinh else b"F"
      params[b"color_is_vector%d" % i] = b"F"
      params[b"brightness%d" % i] = convert_float_to_str(infod.brightness)
      params[b"color_min%d" % i] = convert_float_to_str(infod.color_min)
      params[b"color_max%d" % i] = convert_float_to_str(infod.color_max)

    params[b"xres"] = convert_int_to_str(xres)
    params[b"yres"] = convert_int_to_str(yres)
    if zmin is not None:
        params[b"zmin"] = convert_float_to_str(float(zmin))
    if zmax is not None:
        params[b"zmax"] = convert_float_to_str(float(zmax))
    params[b"fov"] = convert_float_to_str(camera.fov)

    pfile = new paramfile(params)
    pfile.setVerbosity(False)
    pic = new arr2[COLOUR](xres, yres)

    for i in xrange(colormap.cmap.size()):
        colormap.cmap[i].sortMap()

    if backup_positions:
      use_positions = new vector[particle_sim]()
      use_positions.reserve(positions.particle_data.size())
      use_positions.insert(use_positions.end(), positions.particle_data.begin(), positions.particle_data.end())
    else:
      use_positions = positions.particle_data

    campos = camera.campos
    lookat = camera.lookat
    sky = camera.sky
    cmap = colormap.cmap

    with nogil:
      host_rendering(deref(pfile), deref(use_positions),
                   deref(pic), campos, lookat, sky,
                   cmap, python_printer)
    print("Done rendering")

    if backup_positions:
      del use_positions

    xexp = new exptable[npx.float32_t](-20.0)

    a_array = a = np.empty((yres, xres, 3),dtype=np.float32)

    for ix in xrange(xres):
      for iy in xrange(yres):
        a[iy,ix,0] = -xexp.expm1(deref(pic)[ix][iy].r);
        a[iy,ix,1] = -xexp.expm1(deref(pic)[ix][iy].g);
        a[iy,ix,2] = -xexp.expm1(deref(pic)[ix][iy].b);

    del pfile
    del xexp
    del pic

    return a
