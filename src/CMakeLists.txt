include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${NUMPY_INCLUDE_DIRS}  ${PYTHON_INCLUDE_DIRS})

set(CMAKE_SHARED_MODULE_PREFIX)
SET(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -Bsymbolic-functions")
SET(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} ${OpenMP_C_FLAGS}")


if (NOT SYSTEM_PYTHON_SITE_PACKAGES)
  execute_process (COMMAND ${PYTHON_EXECUTABLE} -c "from distutils.sysconfig import get_python_lib; import sys; print(get_python_lib())" OUTPUT_VARIABLE internal_PYTHON_SITE_PACKAGES OUTPUT_STRIP_TRAILING_WHITESPACE)
  SET(SYSTEM_PYTHON_SITE_PACKAGES ${internal_PYTHON_SITE_PACKAGES} CACHE PATH "Path to the target system-wide site-package where to install python modules")
  mark_as_advanced(SYSTEM_PYTHON_SITE_PACKAGES)
endif (NOT SYSTEM_PYTHON_SITE_PACKAGES)

if (NOT USER_PYTHON_SITE_PACKAGES)
  execute_process (COMMAND ${PYTHON_EXECUTABLE} -c "from site import USER_SITE; print(USER_SITE)" OUTPUT_VARIABLE internal_PYTHON_SITE_PACKAGES OUTPUT_STRIP_TRAILING_WHITESPACE)
  SET(USER_PYTHON_SITE_PACKAGES ${internal_PYTHON_SITE_PACKAGES} CACHE PATH "Path to the target user site-package where to install python modules")

  mark_as_advanced(USER_PYTHON_SITE_PACKAGES)
endif (NOT USER_PYTHON_SITE_PACKAGES)


OPTION(INSTALL_PYTHON_LOCAL OFF)
IF (NOT INSTALL_PYTHON_LOCAL)
  SET(PYTHON_SITE_PACKAGES ${SYSTEM_PYTHON_SITE_PACKAGES})
ELSE (NOT INSTALL_PYTHON_LOCAL)
  SET(PYTHON_SITE_PACKAGES ${USER_PYTHON_SITE_PACKAGES})
ENDIF(NOT INSTALL_PYTHON_LOCAL)


#################
# SPLOTCH MODULE

add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/_splotch.cpp
  COMMAND ${CYTHON} --cplus -o ${CMAKE_CURRENT_BINARY_DIR}/_splotch.cpp ${CMAKE_CURRENT_SOURCE_DIR}/_splotch.pyx
  DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/_splotch.pyx)

add_library(_splotch MODULE  ${CMAKE_CURRENT_BINARY_DIR}/_splotch.cpp)
target_link_libraries(_splotch splotch ${PYTHON_LIBRARIES})

#################


INSTALL(TARGETS _splotch 
  LIBRARY DESTINATION ${PYTHON_SITE_PACKAGES}/splotch
)

INSTALL(DIRECTORY splotch DESTINATION ${PYTHON_SITE_PACKAGES} 
        FILES_MATCHING PATTERN "*.py")

