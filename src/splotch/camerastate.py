from . import _splotch as splotch


class CameraState(object):
    def __init__(self):
        self.lookat = [0, 0, 0]
        self.position = [1, 0, 0]
        self.sky = [0, 0, 1]

    def getCamera(self):
        return splotch.SplotchCamera(self.position, self.lookat, self.sky)
