import numpy as np
from . import _splotch as splotch
from .camerastate import CameraState


def make_axis_rotation_matrix(direction, angle):
    """
    Create a rotation matrix corresponding to the rotation around a general
    axis by a specified angle.

    R = dd^T + cos(a) (I - dd^T) + sin(a) skew(d)

    Parameters:

        angle : float a
        direction : array d
    """
    d = np.array(direction, dtype=np.float64)
    d /= np.linalg.norm(d)

    eye = np.eye(3, dtype=np.float64)
    ddt = np.outer(d, d)
    skew = np.array(
        [[0, d[2], -d[1]], [-d[2], 0, d[0]], [d[1], -d[0], 0]], dtype=np.float64
    )

    mtx = ddt + np.cos(angle) * (eye - ddt) + np.sin(angle) * skew
    return mtx


def determineFraction(frame, Nframes, acceleration):
    assert frame < Nframes

    if frame == 0:
        return 0
    if frame == Nframes - 1:
        return 1

    if frame > Nframes / 2:
        frame_t = Nframes - frame
        switch = True
    else:
        frame_t = frame
        switch = False

    epsilon = float(1) / float(Nframes - acceleration)
    f0 = 0.5 * acceleration / float(Nframes - acceleration)
    if acceleration > 0 and frame_t < acceleration:
        a = 0.5 * epsilon / acceleration
        f = a * frame_t**2
    else:
        f = epsilon * (frame_t - acceleration) + f0

    return (1 - f) if switch else f


def cameraMove(
    state=None, fromposition=None, toposition=None, Nframes=2, acceleration=0
):
    """
    Build a series of camera state to bring the camera from its current position to the indicated one using
    a translation.

    Keywords:
      state: camera state
      dfrom: float
      afrom: tuple of two floats
      ato: tuple of two floats
      dto: float
      center: tuple of three floats
      Nframes: int
      acceleration: int number of frames to reach cruise speed
    """
    assert (state is not None) or (fromposition is not None)

    if state is None:
        state = CameraState()

    from_position = np.array(from_position)
    to_position = np.array(to_position)

    for i in range(Nframes):
        out_state = CameraState()

        out_state.lookat = state.lookat
        out_state.sky = state.sky
        f = determineFraction(i, Nframes, acceleration)
        out_state.position = list(f * to_position + (1 - f) * from_position)

        yield out_state


def cameraRotateAndScale(
    state=None,
    dfrom=None,
    afrom=None,
    ato=(45, 360),
    dto=10,
    center=(0, 0, 0),
    Nframes=2,
    acceleration=0,
):
    """
    Build a series of camera state to bring the camera from its current position to the indicated one using
    a rotation.

    Keywords:
      state: camera state
      dfrom: float
      afrom: tuple of two floats
      ato: tuple of two floats
      dto: float
      center: tuple of three floats
      Nframes: int
      acceleration: int number of frames to reach cruise speed

    """
    ## Rotate the camera, make a list of camera state
    assert (state is not None) or ((dfrom is not None) and (afrom is not None))

    if dfrom is None or afrom is None:
        delta = np.array([p - c for p, c in zip(state.position, center)])
        dfrom = np.sqrt((delta**2).sum())
        theta_from = np.arcsin(delta[2] / dfrom)
        phi_from = np.arctan2(delta[1], delta[0])
        phi_from = phi_from % (2 * np.pi)

    else:
        if state is None:
            state = CameraState()
        theta_from, phi_from = np.radians(afrom)

    theta_to, phi_to = np.radians(ato)

    x_from = np.cos(theta_from) * np.cos(phi_from)
    y_from = np.cos(theta_from) * np.sin(phi_from)
    z_from = np.sin(theta_from)

    v_from = np.array([x_from, y_from, z_from])

    x_to = np.cos(theta_to) * np.cos(phi_to)
    y_to = np.cos(theta_to) * np.sin(phi_to)
    z_to = np.sin(theta_to)

    v_to = np.array([x_to, y_to, z_to])

    u = np.cross(v_to, v_from)

    u_norm = np.sqrt((u**2).sum())
    abs_angle = np.arcsin(u_norm)
    if u_norm != 0:
        u /= u_norm
        do_rotation = True
    else:
        do_rotation = False

    for i in range(Nframes):
        out_state = CameraState()

        f = determineFraction(i, Nframes, acceleration)
        if do_rotation:
            M = make_axis_rotation_matrix(u, abs_angle * f)
        else:
            M = np.matrix(np.diag(np.ones(3)))

        d = dfrom + (dto - dfrom) * f
        q = np.array(d * np.matrix(M) * np.matrix(v_from[:, None])).flatten()

        out_state.position = [c + p for c, p in zip(center, q)]
        print("** CAMERA POSITION: %s ** " % repr(out_state.position))
        out_state.lookat = state.lookat
        out_state.sky = state.sky

        yield out_state


def cameraAxialRotate(
    state=None, axis=(0, 0, 1), delta=10.0, center=(0, 0, 0), Nframes=2, acceleration=0
):
    assert state is not None

    delta = np.radians(delta)
    position = np.array(state.position)
    center = np.array(center)
    v = np.matrix((position - center)[:, None])
    for i in range(Nframes):
        out_state = CameraState()

        f = determineFraction(i, Nframes, acceleration)
        M = make_axis_rotation_matrix(axis, -delta * f)

        q = center + np.array(np.matrix(M) * v).flatten()

        out_state.position = list(q)
        out_state.lookat = state.lookat
        out_state.sky = state.sky

        yield out_state


def cameraGoLook(
    state=None,
    go_lookat=(0, 0, 0),
    go_position=(1, 1, 1),
    go_sky=(0, 0, 1),
    Nframes=2,
    acceleration=0,
):
    import numpy as npy

    assert state is not None

    orig_sky = npy.array(state.sky)
    go_sky = npy.array(go_sky)
    for i in range(Nframes):
        out_state = CameraState()

        f = determineFraction(i, Nframes, acceleration)

        out_state.position = [
            (op * (1 - f) + f * np) for op, np in zip(state.position, go_position)
        ]
        out_state.lookat = [
            (op * (1 - f) + f * np) for op, np in zip(state.lookat, go_lookat)
        ]
        sky = orig_sky * (1 - f) + go_sky * f
        out_state.sky = list(sky / npy.linalg.norm(sky, ord=2))
        print(
            "** CAMERA POSITION: %s  LOOK: %s** "
            % (repr(out_state.position), repr(out_state.lookat))
        )

        yield out_state
