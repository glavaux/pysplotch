import numpy as np
from PIL import Image
from . import _splotch as splotch


class SplotchAnim(object):
    """
    Generate a series of pictures from some dataset

    """

    def __init__(
        self, positions, cmap, datasets, width=1280, height=720, base="out%03d.png"
    ):
        """
        Constructor

        Parameters:
          positions: particleArray object
          cmap: SplotchColormap object
          datasets: list of dataDescriptor object

        Keywords:
          width: width of frames (pixels)
          height: height of frames (pixels)
          base: base filename, must contain string with '%d'
        """

        self.cmap = cmap
        self.positions = positions
        self.datasets = datasets
        self.w = width
        self.h = height
        self.pic_id = 0
        self.base = base
        self.zmin = 0
        self.zmax = 1e23
        self.finalReductionFrame = -1
        self.initialReductionFrame = -1

    def _render(self, camera, a=0.02):
        return splotch.splotch(
            self.datasets,
            self.positions,
            camera.getCamera(),
            self.cmap,
            gray_absorption=a,
            xres=self.w,
            yres=self.h,
            backup_positions=True,
            zmin=self.zmin,
            zmax=self.zmax,
        )

    def _newpic(self, *args, **kwargs):
        pic = self._render(*args, **kwargs)
        pic = (pic * 255)[::-1, :, :].astype(np.uint8)

        pilImage = Image.frombuffer(
            "RGB", (self.w, self.h), pic.tostring(), "raw", "RGB", 0, 1
        )
        pilImage.save(self.base % self.pic_id)
        self.pic_id += 1

    def _do_z_twiddle(self):
        f = float(self.pic_id - self.initialReductionFrame) / (
            self.finalReductionFrame - self.initialReductionFrame
        )
        print("Twiddling: f=%g" % f)
        if not self.logReduction:
            if self.target_zmax is not None:
                self.zmax = self.initial_zmax * (1 - f) + self.target_zmax * f
            if self.target_zmin is not None:
                self.zmin = self.initial_zmin * (1 - f) + self.target_zmin * f
        else:
            if self.target_zmax is not None:
                self.zmax = np.exp(self.initial_zmax * (1 - f) + self.target_zmax * f)
            if self.target_zmin is not None:
                self.zmin = np.exp(self.initial_zmin * (1 - f) + self.target_zmin * f)

    def render(self, cam_iter, **kwargs):
        """
        Do the animation production using the given camera iterator
        """
        for campos in cam_iter:
            if self.pic_id < self.finalReductionFrame:
                self._do_z_twiddle()

            self.last_state = campos
            self._newpic(campos, **kwargs)

    def reduceDepthOfField(self, zmin=None, zmax=None, next_frames=1, log=False):
        assert isinstance(log, bool)

        self.target_zmin = zmin
        self.target_zmax = zmax
        self.initialReductionFrame = self.pic_id
        self.initial_zmin = self.zmin
        self.initial_zmax = self.zmax
        self.logReduction = log
        if log:
            self.target_zmin = np.log(self.target_zmin)
            self.target_zmax = np.log(self.target_zmax)
            self.initial_zmin = np.log(self.initial_zmin)
            self.initial_zmax = np.log(self.initial_zmax)

        self.finalReductionFrame = self.pic_id + next_frames
