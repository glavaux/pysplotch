#ifndef __CPPSTRING_HELPER_HPP
#define __CPPSTRING_HELPER_HPP

#include <string>
#include <sstream>

static inline std::string convert_int_to_str(int a)
{
    std::ostringstream oss;
    
    oss << a;
    return oss.str();
}

static inline std::string convert_float_to_str(float a)
{
    std::ostringstream oss;
    
    oss << a;
    return oss.str();
}


#endif
