from PIL import Image
import _splotch
import numpy as np

Np=20000
w,h=800,800

x = _splotch.particleArray(Np)

x[:] = np.random.rand(Np,3)
#x[0] = [0.5,.5,.5]

x.setIntensity(0.01)
x.setRadius(0.005)
x.setValues(np.random.rand(Np))


cmap = _splotch.SplotchColormap(num_types=1)
cmap.addValue(0, 0, (0, 0, 0))
cmap.addValue(0, .5, (.5, 0.5, 0))
cmap.addValue(0, .5, (0, 0.5, 0.5))
cmap.addValue(0, 1, (1, 0., 0))

theta=np.pi/4
r = 2
for i,phi in enumerate(np.arange(0,2*np.pi, 0.05)):

  campos = _splotch.SplotchCamera([r*np.cos(phi)*np.cos(theta)+0.5,r*np.sin(phi)*np.cos(theta)+0.5,r*np.sin(theta)+0.5], [0.5,0.5,0.5], [0,0,1])

  pic = _splotch.splotch(x, campos, cmap, brightness=0.5, gray_absorption=0.2, xres=w, yres=h, backup_positions=True)

  pic = (pic*255).transpose((1,0,2))[::-1,:,:].astype(np.uint8)

  pilImage = Image.frombuffer('RGB', (w,h), pic.tostring(), 'raw', 'RGB', 0, 1)
  pilImage.save('out%03d.png' % i)
