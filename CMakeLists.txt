#
# 
#
#

cmake_minimum_required(VERSION 3.6)
set(CMAKE_CXX_STANDARD 11)


INCLUDE(CheckCCompilerFlag)
include(${CMAKE_SOURCE_DIR}/color_msg.cmake)
include(FindPkgConfig)
include(ExternalProject)
PROJECT(PySplotch)

INCLUDE(FindOpenMP)

SET(PROJECT_VERSION "1.0alpha1")

IF(PKG_CONFIG_ENV)
  SET(ENV{PKG_CONFIG_PATH} ${PKG_CONFIG_ENV})
ENDIF(PKG_CONFIG_ENV)

find_package(PythonInterp)
find_package(PythonLibs)
set(NumPy_FIND_REQUIRED TRUE)
include(${CMAKE_SOURCE_DIR}/FindNumPy.cmake)

cmessage(STATUS "Python include path: ${PYTHON_INCLUDE_PATH}")

find_path(GSL_INCLUDE_PATH NAMES gsl/gsl_blas.h)

find_program(CYTHON cython)

IF (NOT CYTHON)
  MESSAGE(ERROR "Cython compiler has not been found")
ENDIF(NOT CYTHON)

OPTION(ENABLE_OPENMP "Enable OpenMP support" OFF)
IF(ENABLE_OPENMP)

  IF (NOT OPENMP_FOUND)
    MESSAGE(ERROR "No known compiler option for enabling OpenMP")
  ENDIF(NOT OPENMP_FOUND)

  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
  SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKED_FLAGS} ${OpenMP_C_FLAGS}")
  add_definitions(-DOPENMP)

ENDIF(ENABLE_OPENMP)

include(external/external_build.cmake)

subdirs(splotch src)

